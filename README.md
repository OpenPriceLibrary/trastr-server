# Trastr

## What is it?
Trastr is a web app for storing and processing grocery prices to give users insight on where to shop. Visit our instance at [trastr.com](https://trastr.com).

## Where does the data come from?
Food price data is completely crowdsourced. In the "Add Price" dialog, users can easily enter a product's barcode, select a store from the map, and enter the data. Store data is mostly imported from [OpenStreetMap](https://www.openstreetmap.org/) and product data is mostly imported from [OpenFoodFacts](https://world.openfoodfacts.org/).

## What technologies were used?
**Languages**
* Python
* SQL (Postgres)
* Javascript
* HTML
* CSS

**Frameworks**
* Flask
* Bootstrap

**Libraries**
* SQLAlchemy (ORM for PostgreSQL)
* WTForms (validation and styling for web forms)
* Requests (accessing Mapbox and OpenFoodFacts API's)
* Leaflet.js (slippy maps)
* Pint (Mass and volume unit conversion)

## Run it yourself
1. Use a GNU/Linux machine. The project maintainers run [Debian](https://www.debian.org) and some piece of arcane garbage on their development machines. We do not yet support MacOS, Windows, Haiku, 9Front, etc., but invite contributions to development environment documentation.
2. Register an account at [mapbox.com](https://mapbox.com) and create an API key at [account.mapbox.com/access-tokens/create](]https://account.mapbox.com/access-tokens/create/) (we're working on making this part optional).
3. Clone this repository.
4. Install dependencies on your local machine:
   * **Debian**: apt install `vagrant-libvirt` `libvirt-daemon-system` `virt-manager` `ansible`
   * **Ubuntu**: `qemu-kvm` `libvirt-clients` `libvirt-daemon-system` `bridge-utils` `libguestfs-tools` `genisoimage` `virtinst` `lobosinfo-bin` `virt-manager`
5. Next, you will need to add your user to these groups:
    - **Debian** : `libvirt` `libvirt-qemu`
    - **Ubuntu** : `libvirtd` `kvm`
6. Reboot your computer. If you have issues with the libvirt setup, check out [this CTT video](https://www.youtube.com/watch?v=ozYKkaVK0_A).
7. In the project root, create a file called 'vagrant_ansible_vars.yml' with this content:
    ```
    ---
    fqdn: "localhost"
    secret_key: "A RANDOMLY GENERATED SECRET THAT YOU DO NOT SHARE"
    mapbox_api_key: "YOUR MAPBOX API KEY"
    db_password: "A RANDOMLY GENERATED PASSWORD"
    ```
    (Please note that you must replace everything in quotes with your own data.)

8. Run `vagrant up` then `vagrant provision` to set up and run the included Vagrant box
9. Visit http://localhost:8080 to ensure the site is working properly.

## Troubleshooting
- If, after nuking the local repo in a git mishap, `vagrant up` presents `Name <vagrant-domain-name> of domain about to create is already taken. Please try to run
'vagrant up' command again.`:
    - run `vagrant box remove <boxname>`
    - run `sudo virsh undefine <vagrant-domain-name>`
    - run `vagrant up` again

## Deploying to your own server
Please edit `hosts` to reflect the correct IP address and store the appropriate secrets at [ci.codeberg.org](https://ci.codeberg.org).

## How can I contribute?
Follow the steps above to test out trastr-server, then explore the codebase. Try your hand at an issue on [Codeberg](https://codeberg.org/OpenPriceLibrary/trastr-server/issues) or [sourcehut](https://todo.sr.ht/~blendergeek/open-price-library) that interests you!

