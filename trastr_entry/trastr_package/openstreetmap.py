import requests
from geopy.geocoders import Nominatim
from trastr_package import app
import overpy
from time import sleep

def geocode(address):
    # use mapbox geocoding api
    result = requests.get(
        f"https://api.mapbox.com/geocoding/v5/mapbox.places/{address}.json?access_token={app.config['MAPBOX_API_KEY']}"
    )
    # return coordinates from result
    return result.json()["features"][0]["center"][0:2]


def osm_id_to_over_id(osm_id):
    return osm_id + 3600000000


def geocode_boundary(place_name):
    geolocator = Nominatim(user_agent="Trastr")
    geo_results = geolocator.geocode(place_name, exactly_one=False)
    for result in geo_results:
        if result.raw.get("osm_type") == "relation":
            place_relation = result
            break
    if "place_relation" in locals():
        osm_id = place_relation.raw.get("osm_id")
        overpass_id = osm_id_to_over_id(osm_id)
        return overpass_id
    else:
        return None


def geocode_within_boundary(overpass_id, shop_tag_list, timeout=60):
    if overpass_id != None:
        api = overpy.Overpass()
        query_body = ""
        for shop_tag in shop_tag_list:
            query_body += f"node[{shop_tag}](area.searchArea); way[{shop_tag}](area.searchArea); relation[{shop_tag}](area.searchArea);"
        query = f"[out:json][timeout:{timeout}]; area({overpass_id})->.searchArea;({query_body}); out center qt; >;"
        results = api.query(query)
        return results
    else:
        return None


def extract_key_value_pairs(key_value_string):
    # Parses and validates key=value pairs, then formats them
    # for insertion into a
    raw_pairs = key_value_string.replace(" ", "").splitlines()
    validated_formatted_pairs = [
        f"'{item.split('=')[0]}'='{item.split('=')[1]}'"
        for item in raw_pairs
        if item.count("=") == 1 and item[0] != "=" and item[-1] != "="
    ]
    return validated_formatted_pairs


def reverse_geocode(element):
    headers = {'user-agent': 'trastr/0.0.0'}
    if str(type(element)) == "<class 'overpy.Node'>":
        params={'osm_ids': f"N{element.id}", 'format': 'json'}
    elif str(type(element)) == "<class 'overpy.Way'>":
        params={'osm_ids': f"W{element.id}", 'format': 'json'}

    sleep(1)
    response = requests.get("https://nominatim.openstreetmap.org/lookup",
                            params=params,
                            headers=headers)
    return response
