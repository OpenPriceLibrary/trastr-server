import requests


def api(barcode):
    # TODO add timeout to request
    bar_string = str(barcode)
    try:
        result = requests.get(
            f"https://world.openfoodfacts.org/api/v0/product/{bar_string}.json"
        ).json()
        return result
    except Exception as e:
        return 1
