from trastr_package import app, login_manager
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from geoalchemy2.types import Geography
from sqlalchemy import inspect, event, text
from sqlalchemy.sql.expression import and_
from datetime import datetime
from flask_migrate import Migrate

db = SQLAlchemy(app)
migrate = Migrate(app, db)

def database_is_empty():
    engine = db.get_engine(app)
    table_names = inspect(engine).get_table_names()
    is_empty = table_names == []
    return is_empty


def get_latest_user_version_by_username(username):
    """Get latest version of user with given username."""
    # create subquery that gets the user_id and change_dtm for
    # the latest version of each user
    latest_version_by_user = (
        db.session.query(
            UserVersion.user_id,
            db.func.max(UserVersion.change_dtm).label('latest_version_dtm')
        )
        .group_by(UserVersion.user_id)
        .subquery()
    )

    # join subquery table with UserVersion to get additional
    # fields like "username", then filter for username from form
    user_version = (
        db.session.query(UserVersion,latest_version_by_user)
        .select_from(UserVersion)
        .join(
            latest_version_by_user, 
            and_(
                UserVersion.user_id == latest_version_by_user.c.user_id,
                UserVersion.change_dtm == latest_version_by_user.c.latest_version_dtm
            )
        )
        .filter(UserVersion.username == username)
        .first()
        # TODO put a check here that issues 404 and logs incident
        # if more than one record is returned
    )
    if user_version:
        return user_version.UserVersion
    else:
        return None


def get_latest_user_version_by_email(email):
    """Get latest version of user with given email."""
    # create subquery that gets the user_id and change_dtm for
    # the latest version of each user
    latest_version_by_user = (
        db.session.query(
            UserVersion.user_id,
            db.func.max(UserVersion.change_dtm).label('latest_version_dtm')
        )
        .group_by(UserVersion.user_id)
        .subquery()
    )

    # join subquery table with UserVersion to get additional
    # fields like "email", then filter for email from form
    user_version = (
        db.session.query(UserVersion,latest_version_by_user)
        .select_from(UserVersion)
        .join(
            latest_version_by_user, 
            and_(
                UserVersion.user_id == latest_version_by_user.c.user_id,
                UserVersion.change_dtm == latest_version_by_user.c.latest_version_dtm
            )
        )
        .filter(UserVersion.email == email)
        .first()
        # TODO put a check here that issues 404 and logs incident
        # if more than one record is returned
    )
    if user_version:
        return user_version.UserVersion
    else:
        return None


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


user_access_role = db.Table(
    "user_access_role",
    db.Column("user_id", db.BigInteger, db.ForeignKey("user.id"), primary_key=True),
    db.Column(
        "access_role", db.BigInteger, db.ForeignKey("access_role.id"), primary_key=True
    ),
)


access_role_permission = db.Table(
    "access_role_permission",
    db.Column(
        "access_role", db.BigInteger, db.ForeignKey("access_role.id"), primary_key=True
    ),
    db.Column(
        "permission", db.BigInteger, db.ForeignKey("permission.id"), primary_key=True
    ),
)

# The product_attribute table maintains the associations between products and product attributes.
# This table is necessary because there is a many-to-many relationship between products and product attributes.
product_attribute_product_version = db.Table(
    "product_attribute_product_version",
    db.Column(
        "product_attribute", db.BigInteger, db.ForeignKey("product_attribute.id"), primary_key=True
    ),
    db.Column("change_dtm", db.DateTime, primary_key=True),
    db.Column("product_id", db.BigInteger, primary_key=True),
    db.ForeignKeyConstraint(
        ('change_dtm', 'product_id'),
        ('product_version.change_dtm', 'product_version.product_id')
    ),
)

product_attribute_open_food_facts_category = db.Table(
    "product_attribute_open_food_facts_category",
    db.Column(
        "product_attribute", db.BigInteger, db.ForeignKey("product_attribute.id"), primary_key=True
    ),
    db.Column(
        "open_food_facts_category", db.Unicode, db.ForeignKey("open_food_facts_category.name"), primary_key=True
    ),
)

product_version_open_food_facts_category = db.Table(
    "product_version_open_food_facts_category",
    db.Column("change_dtm", db.DateTime, primary_key=True),
    db.Column("product_id", db.BigInteger, primary_key=True),
    db.Column(
        "open_food_facts_category", db.Unicode, db.ForeignKey("open_food_facts_category.name"), primary_key=True
    ),
    db.ForeignKeyConstraint(
        ('change_dtm', 'product_id'),
        ('product_version.change_dtm', 'product_version.product_id')
    ),
)


class MagnitudeUnitType(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    wikidata = db.Column(db.String(20))
    wikipedia = db.Column(db.Unicode(300))
    abbreviation = db.Column(db.Unicode)
    magnitude_units = db.relationship("MagnitudeUnit", backref="magnitude_unit_type")


class MagnitudeUnitSystem(db.Model):
    __tablename__ = "magnitude_unit_system"
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    wikidata = db.Column(db.String(20))
    wikipedia = db.Column(db.Unicode(300))
    abbreviation = db.Column(db.Unicode)
    magnitude_units = db.relationship("MagnitudeUnit", backref="magnitude_unit_system")
    user_versions = db.relationship("UserVersion", backref="magnitude_unit_system")


class MagnitudeUnit(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    wikidata = db.Column(db.String(20))
    wikipedia = db.Column(db.Unicode(300))
    abbreviation = db.Column(db.Unicode)
    magnitude_unit_type_id = db.Column(
        db.BigInteger, db.ForeignKey("magnitude_unit_type.id"), nullable=False
    )
    magnitude_unit_system_id = db.Column(
        db.BigInteger, db.ForeignKey("magnitude_unit_system.id"), nullable=False
    )
    product_versions = db.relationship("ProductVersion", backref="magnitude_unit")


class User(db.Model, UserMixin):
    id = db.Column(db.BigInteger, primary_key=True)
    user_versions = db.relationship("UserVersion", backref="user", order_by="UserVersion.change_dtm.desc()")
    product_versions = db.relationship("ProductVersion", backref="user")
    store_versions = db.relationship("StoreVersion", backref="user")
    prices = db.relationship("Price", backref="user")
    access_roles = db.relationship(
        "AccessRole", secondary=user_access_role, backref="users"
    )

    def encode_password_reset_token(self, lifetime_seconds=600):
        """Generate a JSON Web Signature containing the user's ID."""
        s = Serializer(app.config['SECRET_KEY'], lifetime_seconds)
        token = s.dumps({'user_id': self.id}).decode('utf-8')
        return token
    
    @staticmethod
    def decode_password_reset_token(token):
        """Attempt to get content from JSON Web Signature."""
        s = Serializer(app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        else:
            return User.query.get(user_id)


@event.listens_for(User.__table__, "after_create")
def increase_user_id_autoincrement_start(*args, **kwargs):
    """Set first user ID to 2^32 per suggestion of Evan Priestley."""
    statement = text(f"SELECT setval('user_id_seq', { 2 ** 32 - 1 });")
    db.session.execute(statement)


class UserVersion(db.Model):
    user_id = db.Column(db.BigInteger, db.ForeignKey("user.id"), primary_key=True)
    change_dtm = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, primary_key=True)
    username = db.Column(db.Unicode(), nullable=False)
    password = db.Column(db.Unicode(200), nullable=False)
    name = db.Column(db.Unicode)
    email = db.Column(db.Unicode(200), nullable=False)
    magnitude_unit_system_id = db.Column(
        db.BigInteger, db.ForeignKey("magnitude_unit_system.id"), nullable=False
    )
    location = db.Column(Geography("POINT"))
    #   OSM Wiki - 'Commonly used subkeys'
    addr_housenumber = db.Column(db.Unicode)
    addr_housename = db.Column(db.Unicode)
    #    osm addr:flats is not applicable to a single address
    addr_conscriptionnumber = db.Column(db.Unicode)
    addr_street = db.Column(db.Unicode)
    addr_place = db.Column(db.Unicode)
    addr_postcode = db.Column(db.Unicode)
    addr_city = db.Column(db.Unicode)
    addr_country = db.Column(db.Unicode)
    #   OSM Wiki - 'For countries using, hamlet, subdistrict,
    #   district, province, state'
    addr_hamlet = db.Column(db.Unicode)
    addr_suburb = db.Column(db.Unicode)
    addr_subdistrict = db.Column(db.Unicode)
    addr_province = db.Column(db.Unicode)
    addr_state = db.Column(db.Unicode)
    #   OSM Wiki - 'Detailed subkeys'
    addr_door = db.Column(db.Unicode)
    addr_unit = db.Column(db.Unicode)
    addr_flats = db.Column(db.Unicode)
    addr_floor = db.Column(db.Unicode)
    addr_block = db.Column(db.Unicode)
    local_image_filename = db.Column(db.Unicode(20))
    remote_image_url = db.Column(db.Unicode(300))


class AccessRole(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, unique=True)
    permissions = db.relationship(
        "Permission", secondary=access_role_permission, backref="access_roles"
    )


class Permission(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, unique=True)


class Product(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    product_versions = db.relationship("ProductVersion", backref="product", order_by="ProductVersion.change_dtm.desc()")
    prices = db.relationship("Price", backref="product", order_by="Price.change_dtm.desc()")


class ProductVersion(db.Model):
    product_id = db.Column(db.BigInteger, db.ForeignKey("product.id"), primary_key=True)
    change_dtm = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, primary_key=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey("user.id"), nullable=False)
    # barcode is for storing the numerical result of scanning the primary barcode
    # on the product. It does not need to be a GTIN, EAN, or anything like that.
    barcode = db.Column(db.BigInteger)
    category_id = db.Column(db.BigInteger, db.ForeignKey("product_category.id"))
    name = db.Column(db.Unicode(80))
    brand_id = db.Column(db.BigInteger, db.ForeignKey("product_brand.id"))
    #    maybe have separate volume and mass fields
    magnitude = db.Column(db.Float)
    magnitude_unit_id = db.Column(db.BigInteger, db.ForeignKey("magnitude_unit.id"))
    local_image_filename = db.Column(db.Unicode(20))
    remote_image_url = db.Column(db.Unicode(300))
    open_food_facts_categories = db.relationship(
        "OpenFoodFactsCategory", secondary=product_version_open_food_facts_category, backref="products"
    )
    product_attributes = db.relationship(
        "ProductAttribute", secondary=product_attribute_product_version, backref="products"
    )


class ProductBrand(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    product_versions = db.relationship("ProductVersion", backref="product_brand")


class ProductCategory(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    parent_id = db.Column(db.BigInteger, db.ForeignKey("product_category.id"))
    name = db.Column(db.Unicode, nullable=False)
    off_name = db.Column(db.Unicode)
    wikidata = db.Column(db.Unicode)
    product_versions = db.relationship("ProductVersion", backref="product_category")
    open_food_facts_categories = db.relationship("OpenFoodFactsCategory", backref="product_category")

class OpenFoodFactsCategory(db.Model):
    #id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False, primary_key=True)
    product_category_id = db.Column(db.BigInteger, db.ForeignKey("product_category.id"))
    product_attributes = db.relationship(
        "ProductAttribute", secondary=product_attribute_open_food_facts_category, backref="open_food_facts_categories"
    )

class ProductAttribute(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)

class Store(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    store_versions = db.relationship("StoreVersion", backref="store")
    prices = db.relationship("Price", backref="store")


class StoreVersion(db.Model):
    store_id = db.Column(db.BigInteger, db.ForeignKey("store.id"), primary_key=True)
    change_dtm = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, primary_key=True)
    user_id = db.Column(db.BigInteger, db.ForeignKey("user.id"), nullable=False)
    name = db.Column(db.Unicode(80))
    brand_id = db.Column(db.BigInteger, db.ForeignKey("store_brand.id"))
    category_id = db.Column(db.BigInteger, db.ForeignKey("store_category.id"))
    location = db.Column(Geography("POINT"))
    #   Chose BigInteger type because it is used for IDs in OSM's internal schema
    osm_id = db.Column(db.BigInteger)
    #    floor_area Integer
    #   OSM Wiki - 'Commonly used subkeys'
    addr_housenumber = db.Column(db.Unicode)
    addr_housename = db.Column(db.Unicode)
    #    osm addr:flats is not applicable to a single address
    addr_conscriptionnumber = db.Column(db.Unicode)
    addr_street = db.Column(db.Unicode)
    addr_place = db.Column(db.Unicode)
    addr_postcode = db.Column(db.Unicode)
    addr_city = db.Column(db.Unicode)
    addr_country = db.Column(db.Unicode)
    #   OSM Wiki - 'For countries using, hamlet, subdistrict,
    #   district, province, state'
    addr_hamlet = db.Column(db.Unicode)
    addr_suburb = db.Column(db.Unicode)
    addr_subdistrict = db.Column(db.Unicode)
    addr_province = db.Column(db.Unicode)
    addr_state = db.Column(db.Unicode)
    #   OSM Wiki - 'Detailed subkeys'
    addr_door = db.Column(db.Unicode)
    addr_unit = db.Column(db.Unicode)
    addr_floor = db.Column(db.Unicode)
    addr_block = db.Column(db.Unicode)


class StoreBrand(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    store_versions = db.relationship("StoreVersion", backref="store_brand")


class StoreCategory(db.Model):
    #TODO create OSM tag column here and adjust view functions accordingly
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode)
    store_versions = db.relationship("StoreVersion", backref="store_category")


class Price(db.Model):
    user_id = db.Column(db.BigInteger, db.ForeignKey("user.id"), nullable=False)
    product_id = db.Column(db.BigInteger, db.ForeignKey("product.id"), primary_key=True)
    store_id = db.Column(db.BigInteger, db.ForeignKey("store.id"), primary_key=True)
    change_dtm = db.Column(db.DateTime, default=datetime.utcnow, primary_key=True)
    #   Made price a Numeric type with 15 places on the left and 5 places
    #   on the right to account for runaway inflation and small
    #   denominations respectively.
    price = db.Column(db.Numeric(15, 5), nullable=False)
    currency_id = db.Column(db.BigInteger, db.ForeignKey("currency.id"), nullable=False)


class Currency(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    name = db.Column(db.Unicode, nullable=False)
    iso_4217_alpha_code = db.Column(db.String(3))
    prices = db.relationship("Price", backref="currency")
