import copy

from trastr_package import (
    app,
    bcrypt,
    mail,
    openfoodfacts,
    access
)
from flask_mail import Message
from trastr_package.url import is_safe_url
import shapely
from geoalchemy2.shape import to_shape
from geoalchemy2.functions import ST_DWithin, ST_GeomFromEWKT
from geoalchemy2.elements import WKTElement
from geoalchemy2.comparator import Comparator
from sqlalchemy.sql.expression import and_
from trastr_package.barcodes import is_upc, is_ean
from sqlalchemy import exc

# TODO: import db from __Init__ instead
from trastr_package.db import (
    db,
    database_is_empty,
    get_latest_user_version_by_username,
    get_latest_user_version_by_email,
    User,
    UserVersion,
    AccessRole,
    OpenFoodFactsCategory,
    Permission,
    Product,
    ProductAttribute,
    ProductVersion,
    ProductBrand,
    ProductCategory,
    Store,
    StoreVersion,
    StoreBrand,
    StoreCategory,
    Price,
    Currency,
    MagnitudeUnit,
    MagnitudeUnitType,
    MagnitudeUnitSystem,
)

from flask import (
    render_template,
    make_response,
    request,
    redirect,
    url_for,
    flash,
    send_from_directory,
    abort,
    jsonify
)
from urllib.parse import quote_plus

from trastr_package.forms import (
    SignUpForm,
    EditUserForm,
    EditPasswordForm,
    LogInForm,
    AddProductForm,
    AddStoreForm,
    AddPriceForm,
    UploadPricesForm,
    ImportStoresForm,
    FindProductForm,
    FindProductsForm,
    FindStoresForm,
    AddRoleForm,
    EditRoleForm,
    EnterEmailForm,
    ResetPasswordForm
)
from flask_login import login_user, current_user, logout_user, login_required

from trastr_package.openstreetmap import (
    geocode,
    geocode_boundary,
    geocode_within_boundary,
    extract_key_value_pairs,
    reverse_geocode,
)

import re

# related to pint library, but may not be necessary any longer
from . import ureg, Q_


@app.route("/")
def root():
    if database_is_empty():  # If there is no database
        return redirect(url_for("setup"))  # Run the setup wizard
    else:
        return redirect(url_for("find_product"))


@app.route("/sign_up", methods=["GET", "POST"])
def sign_up():
    if current_user.is_authenticated:
        return redirect(url_for("find_product"))
    else:
         
        # gather magnitude units from database to show in form
        magnitude_unit_systems = []
        for result in MagnitudeUnitSystem.query.all():
            magnitude_unit_systems.append(
                (
                    result.id,
                    f"{result.name} ({result.abbreviation})",
                )
            )
        form = SignUpForm()
        form.magnitude_unit_system.choices = magnitude_unit_systems

        if not form.validate_on_submit():
            return render_template("sign_up.html", form=form)
        else:
            new_user = User()
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
                "utf-8"
            )
            
            # locate and apply 'Basic' role to first user object
            basic_role = AccessRole.query.filter(AccessRole.name == "Basic").first()
            new_user.access_roles.append(basic_role)

            db.session.add(new_user)
            db.session.flush()

            # Add a user residing in the United States (and first user version)
            new_user_version = UserVersion(
                user = new_user,
                username = form.username.data,
                password = hashed_password,
                email = form.email.data,
                addr_housenumber = form.addr_housenumber.data,
                addr_street = form.addr_street.data,
                addr_unit = form.addr_unit.data,
                addr_city = form.addr_city.data,
                addr_state = form.addr_state.data,
                addr_postcode = form.addr_postcode.data,
                magnitude_unit_system_id = form.magnitude_unit_system.data,
            )
            db.session.add(new_user_version)
            db.session.commit()
            flash("Account created.", "success")
            return redirect(url_for("log_in"))


@app.route("/sign_up_first_user", methods=["GET", "POST"])
def sign_up_first_user():
    if len(User.query.all()) == 0:
        # gather magnitude units from database to show in form
        magnitude_unit_systems = []
        for result in MagnitudeUnitSystem.query.all():
            magnitude_unit_systems.append(
                (
                    result.id,
                    f"{result.name} ({result.abbreviation})",
                )
            )
        form = SignUpForm()
        form.magnitude_unit_system.choices = magnitude_unit_systems

        if not form.validate_on_submit():
            return render_template("sign_up.html", form=form)
        else:
            hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
                "utf-8"
            )

            first_user = User()

            # locate and apply 'Administrator' role to first user object
            administrator_role = AccessRole.query.filter(AccessRole.name == "Administrator").first()
            first_user.access_roles.append(administrator_role)

            db.session.add(first_user)
            db.session.flush()

            # add first user with administrator role
            first_user_version = UserVersion(
                user = first_user,
                username = form.username.data,
                password = hashed_password,
                email = form.email.data,
                addr_housenumber = form.addr_housenumber.data,
                addr_street = form.addr_street.data,
                addr_unit = form.addr_unit.data,
                addr_city = form.addr_city.data,
                addr_state = form.addr_state.data,
                addr_postcode = form.addr_postcode.data,
                magnitude_unit_system_id = form.magnitude_unit_system.data
            )
            db.session.add(first_user_version)
            db.session.commit()
            flash("Account created with 'Administrator' role.", "success")
            return redirect(url_for("log_in"))
    else:
        flash("First user has already been created. Doing nothing.", "warning")
        return redirect(request.referrer)

@app.cli.command("mag_test")
def mag_test():
    mass_results = (
        db.session.query(MagnitudeUnit, MagnitudeUnitType, MagnitudeUnitSystem)
        .join(MagnitudeUnit.magnitude_unit_type)
        .join(MagnitudeUnit.magnitude_unit_system)
        .all()
    )
    for m in mass_results:
        print(dir(m))
        print("")


@app.route("/log_in", methods=["GET", "POST"])
def log_in():
    # redirect user to the dashbord if they are already logged in
    if current_user.is_authenticated:
        return redirect(url_for("find_product"))
    else:
        # instantiate form object to be passed to template
        form = LogInForm()
        if not form.validate_on_submit():
            return render_template("log_in.html", form=form)
        else:
            user_version = get_latest_user_version_by_username(form.username.data)

            # check password in form against password in database
            if user_version and bcrypt.check_password_hash(
                user_version.password, form.password.data
            ):
                # log the user in
                login_user(user_version.user, remember=form.remember.data)
                flash("Log in successful", "success")

                # redirect to /setup if only 1 user account has been created
                if len(User.query.all()) == 1:
                    return redirect(url_for("setup"))
                else:
                    next = request.args.get('next')
                    if not is_safe_url(next):
                        return abort(400)
                    else:
                        return redirect(next or url_for("find_product"))
            else:
                flash("Log in failed", "danger")
                return render_template("log_in.html", form=form)


@app.route("/log_out")
def log_out():
    logout_user()
    flash("Log out successful", "success")
    return redirect(url_for("find_product"))


@app.route("/request_password_reset", methods=["GET", "POST"])
def request_password_reset():
    """
    Display email entry form and send password reset email when submitted.
    """
    form = EnterEmailForm()
    if not form.validate_on_submit():
        return render_template("enter_email.html", form=form)
    else:
        sender = app.config["MAIL_USERNAME"]
        recipient = form.email.data
        user = get_latest_user_version_by_email(recipient).user
        token = user.encode_password_reset_token()
        message = Message('Password Reset Request', sender=sender, recipients=[recipient])
        message.body = f"To reset your password, visit this link:\n{url_for('reset_password', token=token, _external=True)}"
        try:
            mail.send(message)
            flash(f"Password reset email has been sent to {recipient}", "success")
        except:
            flash("Unable to send password reset email due to an error", "danger") 
        return redirect(url_for("log_in"))

@app.route("/reset_password/<token>", methods=["GET", "POST"])
def reset_password(token):
    """
    Display password reset form for users clicking temporary link, and reset password on valid submission.
    """
    
    user = User.decode_password_reset_token(token)
    user_version = user.user_versions[0]
    
    if user is None:
        flash('Password reset token has expired or is invalid', 'warning')
        return redirect(url_for("request_password_reset"))
    
    form = ResetPasswordForm()
    
    if not form.validate_on_submit():
        return render_template("reset_password.html", form=form)
    else:
        hashed_password = bcrypt.generate_password_hash(form.new_password.data).decode(
            "utf-8"
        )

        # create new user version
        new_user_version = UserVersion(user=user,
            username = user_version.username,
            password = hashed_password,
            email = user_version.email,
            addr_housenumber = user_version.addr_housenumber,
            addr_street = user_version.addr_street,
            addr_unit = user_version.addr_unit,
            addr_city = user_version.addr_city,
            addr_state = user_version.addr_state,
            addr_postcode = user_version.addr_postcode,
            magnitude_unit_system_id = user_version.magnitude_unit_system_id
        )

        db.session.add(new_user_version)
        db.session.commit()
        flash("Password has been reset", "success")
        return redirect(url_for("log_in"))


@app.route("/find_product", methods=["GET", "POST"])
@access.permission_required("View Product")
# TODO determine why find_product never returns any results
def find_product():
    form = FindProductForm()
    query = request.args.get("query")

    if form.validate_on_submit():
        return redirect(f"/find_product?query={quote_plus(form.query.data)}")
    else:
        if query:
            if is_upc(query) or is_ean(query):
                products = (
                    db.session.query(Product, ProductVersion)
                    .join(ProductVersion)
                    .order_by("product_id", ProductVersion.change_dtm.desc())
                    .distinct("product_id")
                    .filter(ProductVersion.barcode == query)
                    .all()
                )
                if len(products) == 0:
                    return redirect(f"/import_product/{query}")
                elif len(products) == 1:
                    product = products[0]
                    return redirect(f"/view_product/{product.Product.id}")
                else:
                    return render_template(
                        "find_product.html", form=form, products=products
                    )
            else:
                products = (
                    db.session.query(Product, ProductVersion)
                    .join(ProductVersion)
                    .order_by("product_id", ProductVersion.change_dtm.desc())
                    .distinct("product_id")
                    .filter(ProductVersion.name.ilike(query))
                    .all()
                )
                if len(products) == 0:
                    return render_template("find_product.html", form=form)
                    flash("No products found", "warning")
                else:
                    return render_template(
                        "find_product.html", form=form, products=products
                    )
        else:
            return render_template("find_product.html", form=form)


@app.route("/find_products", methods=["GET", "POST"])
@access.permission_required("View Product")
def find_products():
    """
    This route allows the user to search for products and displays the current prices
    for queried products at various stores.
    Currently the user can only search by name and there are no location filters.
    The default (and only option) for ranking products is price/magnitude with
    no respect for various units.
    """
    form = FindProductsForm()
    args = request.args.to_dict(flat=False)
    name = None
    if 'name' in args:
        name = f"%{args['name'][0]}%"
    if 'attributes' in args:
        for attribute in args['attributes']:
            flash(attribute+', ')
    if 'category' in args:
        flash(args['category'])

    # This query fetches the latest version of each product
    products_sq = (
        db.session.query(Product.id.label("product_id"), ProductVersion.name.label("product_name"), ProductVersion.magnitude.label("product_magnitude"))
        .join(ProductVersion, Product.id == ProductVersion.product_id)
        .distinct(ProductVersion.product_id)
        .order_by(ProductVersion.product_id, ProductVersion.change_dtm.desc())
    )

    if name != None:  # Here we filter the products by name
        products_sq = products_sq.filter(ProductVersion.name.ilike(name))
    products_sq = products_sq.cte('products_sq')

    # This query fetches the latest version of each store
    stores_sq = (
        db.session.query(Store.id.label("store_id"), StoreVersion.name.label("store_name"))
        .join(StoreVersion, Store.id == StoreVersion.store_id)
        .order_by("store_id", StoreVersion.change_dtm.desc())
        .distinct("store_id")
    )
    stores_sq = stores_sq.cte('stores_sq')

    # This combines the previous two queries with the prices table and selects the most
    # recent price for each product at each store
    products = (
        db.session.query(Price.product_id, Price.store_id, Price.change_dtm.label("price_change_dtm"), Price.price, Price.currency_id, products_sq.c.product_name, stores_sq.c.store_name, products_sq.c.product_magnitude)
        .join(products_sq, Price.product_id == products_sq.c.product_id)
        .join(stores_sq, Price.store_id == stores_sq.c.store_id)
        .distinct(Price.store_id, Price.product_id)
        .order_by(Price.store_id, Price.product_id, Price.change_dtm.desc())
    )
    unsorted = products.cte('unsorted')

    # And finally we sort the products based on unit price (ignoring variant unit systems)
    prices = db.session.query(unsorted).order_by(unsorted.c.price/unsorted.c.product_magnitude).all()
    return render_template("find_products.html", products=prices, form=form)

# REQUIRES PERMISSION: ADD_USER
@app.route("/add_user", methods=["GET", "POST"])
@access.permission_required("Add User")
def add_user():
    magnitude_unit_systems = []
    for result in MagnitudeUnitSystem.query.all():
        magnitude_unit_systems.append(
            (
                result.id,
                f"{result.name} ({result.abbreviation})",
            )
        )
    # return str(magnitude_unit_systems)
    form = SignUpForm()
    form.magnitude_unit_system.choices = magnitude_unit_systems

    if not form.validate_on_submit():
        return render_template("sign_up.html", form=form)
    else:
        new_user = User()
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode(
            "utf-8"
        )

        # locate and apply 'Basic' role to first user object
        basic_role = AccessRole.query.filter(AccessRole.name == "Basic").first()
        new_user.access_roles.append(basic_role)

        db.session.add(new_user)
        db.session.flush()

        # Add a user residing in the United States (and first user version)
        new_user_version = UserVersion(
            user=new_user,
            username=form.username.data,
            password=hashed_password,
            email=form.email.data,
            magnitude_unit_system_id=form.magnitude_unit_system.data,
        )
        db.session.add(new_user_version)
        db.session.commit()
        flash(f"Account created! Username: {form.username.data}", "success")
        return redirect(url_for("add_user"))


@app.route("/view_user")
@login_required
def view_user_self():
    """View user account that is currently logged in."""
    user = current_user
    if user:
        latest_user_version = user.user_versions[0]
        return render_template("view_user.html", user=latest_user_version)
    else:
        return abort(404)


@app.route("/view_user/<user_id>")
@login_required
@access.permission_required("View User")
def view_user(user_id):
    """View any user account."""
    user = User.query.filter(User.id == user_id).first()
    if user:
        latest_user_version = user.user_versions[0]
        return render_template("view_user.html", user=latest_user_version)
    else:
        return abort(404)


@app.route("/view_user_versions")
@login_required
def view_user_versions_self():
    """View user versions of account that is currently logged in."""
    user_versions = current_user.user_versions
    return render_template("view_user_versions.html", user_versions = user_versions)


@app.route("/edit_user", methods=["GET", "POST"])
@login_required
def edit_user_self(): 
    """Edit user account that is currently logged in."""
    # get latest version of currently logged in user
    user_version = current_user.user_versions[0]

    form = EditUserForm()
    # gather magnitude units from database to show in form
    magnitude_unit_systems = []
    for result in MagnitudeUnitSystem.query.all():
        magnitude_unit_systems.append(
            (
                result.id,
                f"{result.name} ({result.abbreviation})",
            )
        )
    form.magnitude_unit_system.choices = magnitude_unit_systems

    if not form.validate_on_submit():
        form.username.data = user_version.username
        form.email.data = user_version.email
        form.addr_housenumber.data = user_version.addr_housenumber
        form.addr_street.data = user_version.addr_street
        form.addr_unit.data = user_version.addr_unit
        form.addr_city.data = user_version.addr_city
        form.addr_state.data = user_version.addr_state
        form.addr_postcode.data = user_version.addr_postcode
        form.magnitude_unit_system.data = user_version.magnitude_unit_system.id
        return render_template("edit_user.html", form=form)
    else:
        # create new user version
        new_user_version = UserVersion(user=current_user,
            username = form.username.data,
            password = user_version.password,
            email = form.email.data,
            addr_housenumber = form.addr_housenumber.data,
            addr_street = form.addr_street.data,
            addr_unit = form.addr_unit.data,
            addr_city = form.addr_city.data,
            addr_state = form.addr_state.data,
            addr_postcode = form.addr_postcode.data,
            magnitude_unit_system_id = form.magnitude_unit_system.data
        )

        db.session.add(new_user_version)
        db.session.commit()

        flash("Changes saved", "success")
        return redirect("/view_user")


@app.route("/edit_password", methods=["GET", "POST"])
@login_required
def edit_password_self():
    """Edit password of user account that is currently logged in."""
    # get latest version of currently logged in user
    user_version = current_user.user_versions[0]

    form = EditPasswordForm()
    if not form.validate_on_submit():
        return render_template("edit_password.html", form=form)
    else:
        hashed_password = bcrypt.generate_password_hash(form.new_password.data).decode(
            "utf-8"
        )

        # create new user version
        new_user_version = UserVersion(user=current_user,
            username = user_version.username,
            password = hashed_password,
            email = user_version.email,
            addr_housenumber = user_version.addr_housenumber,
            addr_street = user_version.addr_street,
            addr_unit = user_version.addr_unit,
            addr_city = user_version.addr_city,
            addr_state = user_version.addr_state,
            addr_postcode = user_version.addr_postcode,
            magnitude_unit_system_id = user_version.magnitude_unit_system_id
        )

        db.session.add(new_user_version)
        db.session.commit()

        flash("Password change saved", "success")
        return redirect("/view_user")
    

@app.route("/add_product", methods=["GET", "POST"])
@access.permission_required("Add Product")
def add_product():
    form = AddProductForm()

    # Query for fresh list of Magnitude units to show user
    unit_results = (
        db.session.query(MagnitudeUnit, MagnitudeUnitSystem)
        .join(MagnitudeUnitSystem)
        .all()
    )
    units = []
    for result in unit_results:
        units.append(
            (
                result.MagnitudeUnit.id,
                f"{result.MagnitudeUnit.name} ({result.MagnitudeUnitSystem.abbreviation})",
            )
        )
    # TODO prioritize user's preferred unit system in list
    form.magnitude_unit.choices = units

    # Query for list of product categories to show user
    category_results = db.session.query(ProductCategory).all()
    categories = []
    for result in category_results:
        categories.append(
            (
                result.id,
                f"{result.name}",
            )
        )
    form.category.choices = categories

    if not form.validate_on_submit():
        return render_template("add_product.html", form=form)
    else:
        # TODO: remove str conversion for barcode
        # TODO: find less risky way to extract api data than consecutive get()s
        try:
            off_request = openfoodfacts.api(form.barcode.data).get("product", {})
            name = off_request.get("product_name")
            remote_image_url = off_request.get("image_front_url")
        except Exception as e:
            name = None
            remote_image_url = None

        product = Product()
        db.session.add(product)
        db.session.flush()

        product_version = ProductVersion(
            product_id=product.id,
            user_id=current_user.id,
            barcode=form.barcode.data,
            category_id=form.category.data,
            name=name,
            magnitude=form.magnitude.data,
            magnitude_unit_id=form.magnitude_unit.data,
            remote_image_url=remote_image_url,
        )
        db.session.add(product_version)
        db.session.commit()
        return redirect(f"/view_product/{product.id}")


@app.route("/view_product/<product_id>")
@access.permission_required("View Product")
def view_product(product_id):
    if (not product_id.isdigit()): # If the product_id provided is not an integer, we can't use it.
        title = "Not found"
        content = f"{product_id} is not a valid product_id."
        return render_template("error.html", title=title, content=content), 404
    product = (
        Product.query.join(ProductVersion).filter(Product.id == product_id).first()
    )
    if (product == None): # If database does not return a product, show a helpful error message.
        title = "Not found"
        content = f"The product with the ID {product_id} does not exist."
        return render_template("error.html", title=title, content=content), 404
    latest_product_version = product.product_versions[0]
    
    # get user's current location if the user is authenticated
    if current_user.is_authenticated == True:
        user_location = (
            current_user
            .user_versions[0]
            .location
        )
        # If the user doesn't have a location, use the "default" location
        if not user_location:
            user_location = WKTElement(app.config["DEFAULT_LOCATION_EWKT"], extended=True)
    # If the user isn't logged in, use the default location
    else:
        user_location = WKTElement(app.config["DEFAULT_LOCATION_EWKT"], extended=True)
        
    # show prices for product at local stores from lowest to highest amount
    latest_price_by_product_and_store = (
        db.session.query(
            Price.product_id,
            Price.store_id,
            db.func.max(Price.change_dtm).label('latest_price_dtm')
        )
        .group_by(Price.product_id, Price.store_id)
        .subquery()
    )

    latest_version_by_store = (
        db.session.query(
            StoreVersion.store_id,
            db.func.max(StoreVersion.change_dtm).label('latest_version_dtm')
        )
        .group_by(StoreVersion.store_id)
        .subquery()
    )

    prices = (
        db.session.query(Price, StoreVersion, latest_price_by_product_and_store, latest_version_by_store)
        .join(
            latest_price_by_product_and_store,
            and_(
                Price.product_id == latest_price_by_product_and_store.c.product_id,
                Price.store_id == latest_price_by_product_and_store.c.store_id,
                Price.change_dtm == latest_price_by_product_and_store.c.latest_price_dtm
            )
        )
        .join(
            latest_version_by_store,
            Price.store_id == latest_version_by_store.c.store_id
        )
        .join(
            StoreVersion,
            StoreVersion.store_id == latest_version_by_store.c.store_id
        )
        .filter(Price.product_id == product_id)
        .filter(ST_DWithin(StoreVersion.location, user_location, 24140))
        .order_by(Price.price.asc())
        .limit(10)
        .all()
    ) 
    
  
    return render_template(
        "view_product.html",
        product = latest_product_version,
        prices = prices
    )


@app.route("/view_product")
@access.permission_required("View Product")
def view_product_invalid():
    flash("That product does not exist", "warning")
    return redirect(url_for("find_product"))


@app.route("/add_price", methods=["GET", "POST"])
@login_required
@access.permission_required("Add Price")
def add_price():
    form = AddPriceForm()
    # get list of supported currencies and add to form choices
    currencies = []
    for result in Currency.query.all():
        currencies.append(
            (
                result.id,
                f"{result.name} ({result.iso_4217_alpha_code})",
            )
        )
    form.currency.choices = currencies
    if not form.validate_on_submit():
        # get user's current location
        user_location = (
            current_user
            .user_versions[0]
            .location
        )
        if not user_location:
            user_location = WKTElement(app.config["DEFAULT_LOCATION_EWKT"], extended=True)

        product_id = request.args.get("product_id", None) 
        if product_id and product_id.isdigit():
            form.product_id.data = product_id
            product = (
                Product.query.join(ProductVersion)
                .filter(Product.id == product_id)
                .first()
            )
        else:
            product = None
        return render_template(
            "add_price.html",
            mapbox_api_key=app.config["MAPBOX_API_KEY"],
            map_center=to_shape(user_location),
            form=form,
            product=product,
            product_id=product_id
        )
    elif request.method == "POST":
        try: # Because this can raise an exception, we must use exception handling
            price = Price(
                price=form.price.data,
                product_id=form.product_id.data,
                store_id=form.store_id.data,
                user_id=current_user.id,
                currency_id=form.currency.data
            )
            db.session.add(price)
            db.session.commit()
        # Currently I am catching all exceptions and giving the user a generic answer.
        # This is better than throwing the dreaded 500 error.
        # If the user just uses the app normally, they will never see this error.
        except exc.SQLAlchemyError: 
            db.session.rollback()
            return make_response(
                render_template(
                    "error.html",
                    title="Database Error",
                    content=("The database was unable to complete your request.<br>"
                             "This is often due to referencing objects not in the database")
                ),
                409
            )
        product_id = form.product_id.data
        return redirect(f"/view_product/{product_id}")
    else:
        return make_response(
            render_template(
                "error.html",
                title="GET request not permitted",
                content="Only POST requests may be used to submit data."
            ),
            405
        )

@app.route("/setup")
def setup():
    """The setup tool returns a page of links to setup functions."""
    return render_template("setup.html")


# This route initializes the database if the database is currently empty
@app.route("/init_db")
def init_db():
    # TODO: remove table dropping from web UI
    is_empty = database_is_empty()
    if is_empty:
        db.create_all()
        flash("Initializing database. Done", "success")
    else:
        flash("Database already created. Doing nothing.", "warning")
    return redirect(url_for("setup"))


@app.cli.command("init_db")
def init_db_cli():
    def drop_and_create_db(db):
        print("Dropping database ... Done")
        db.drop_all()
        print("Dropping database ... Done")
        db.create_all()
        print("Initializing database ... Done")

    if database_is_empty():
        drop_and_create_db(db)
    else:
        confirmation_string = "Peter Piper picked a peck of pickled peppers."
        user_string = input(
            f"Database already created! Enter '{confirmation_string}' to overwrite: "
        )
        if user_string == confirmation_string:
            drop_and_create_db(db)
        else:
            print("Confirmation entry incorrect. Doing nothing.")


@app.route("/populate_product_categories")
def populate_product_categories():
    categories = ProductCategory.query.all()

    if len(categories) == 0:
        dairy = ProductCategory(name="Dairy")
        db.session.add(dairy)

        bread = ProductCategory(name="Bread")
        db.session.add(bread)

        meat = ProductCategory(name="Meat")
        db.session.add(meat)

        produce = ProductCategory(name="Produce")
        db.session.add(produce)

        drinks = ProductCategory(name="Drinks")
        db.session.add(drinks)

        baking = ProductCategory(name="Baking")
        db.session.add(baking)

        db.session.commit()
        flash("Populating product categories ... Done", "success")
    else:
        flash("Product categories already created. Doing nothing.", "warning")

    return redirect(url_for("setup"))


@app.route("/populate_store_categories")
def populate_store_categories():
    categories = StoreCategory.query.all()

    if len(categories) == 0:
        supermarket = StoreCategory(name="Supermarket")
        db.session.add(supermarket)

        convenience = StoreCategory(name="Convenience Store")
        db.session.add(convenience)

        variety_store = StoreCategory(name="Variety Store")
        db.session.add(variety_store)

        general = StoreCategory(name="General Store")
        db.session.add(general)

        department_store = StoreCategory(name="Department Store")
        db.session.add(department_store)

        wholesale = StoreCategory(name="Wholesale Store")
        db.session.add(wholesale)

        db.session.commit()
        flash("Populating store categories ... Done", "success")
    else:
        flash("Store categories already created. Doing nothing.", "warning")

    return redirect(url_for("setup"))


@app.route("/populate_currencies")
def populate_currencies():
    """
    This is actually just a placeholder function.
    Right now it creates one (1) currency, the US Dollar.
    Eventually we will need to add more currencies.
    """
    currencies = Currency.query.all()

    if len(currencies) == 0:
        usd = Currency(
            name="US Dollar",
            iso_4217_alpha_code="USD",
        )
        db.session.add(usd)
        db.session.commit()
        flash("Populating currencies ... Done", "success")
    else:
        flash("Currencies already created. Doing nothing.", "warning")

    return redirect(url_for("setup"))


@app.route("/populate_units")
def populate_units():

    # TODO: examine exact definitions of US food labeling units
    # https://www.ecfr.gov/cgi-bin/text-idx?node=se21.2.101_19
    # "(viii) For nutrition labeling purposes, a teaspoon means 5 milliliters (mL), a tablespoon means 15 mL, a cup means 240 mL, 1 fl oz means 30 mL, and 1 oz in weight means 28 g."

    # Define types of units
    units = MagnitudeUnit.query.all()

    if len(units) == 0:
        volume = MagnitudeUnitType(
            name="Volume",
            abbreviation="V",
            wikidata="Q39297",
            wikipedia="https://en.wikipedia.org/wiki/Volume",
        )
        db.session.add(volume)
        mass = MagnitudeUnitType(
            name="Mass",
            abbreviation="m",
            wikidata="Q11423",
            wikipedia="https://en.wikipedia.org/wiki/Mass",
        )
        db.session.add(mass)
        each = MagnitudeUnitType(
            name="Each", abbreviation="ea.", wikidata="Q2360918", wikipedia="i"
        )
        db.session.add(each)
        db.session.commit()

        us_customary = MagnitudeUnitSystem(
            name="United States Customary Units",
            abbreviation="US",
            wikidata="Q1116609",
            wikipedia="https://en.wikipedia.org/wiki/United_States_customary_units",
        )
        db.session.add(us_customary)
        us_food = MagnitudeUnitSystem(
            name="United States Nutrition Labeling Units",
            abbreviation="US Food",
            wikidata="Q69350622",
            wikipedia="",
        )
        db.session.add(us_food)
        imperial = MagnitudeUnitSystem(
            name="Imperial",
            abbreviation="imp",
            wikidata="Q641227",
            wikipedia="https://en.wikipedia.org/wiki/Imperial_units",
        )
        db.session.add(imperial)
        si = MagnitudeUnitSystem(
            name="International System of Units",
            abbreviation="SI",
            wikidata="Q12457",
            wikipedia="https://en.wikipedia.org/wiki/International_System_of_Units",
        )
        db.session.add(si)
        db.session.commit()

        # Define units
        # US customary units
        us_gallon = MagnitudeUnit(
            name="Gallon",
            abbreviation="gal",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="Q23925413",
            wikipedia="https://en.wikipedia.org/wiki/Gallon#US_liquid_gallon",
        )
        db.session.add(us_gallon)
        db.session.commit()

        us_quart = MagnitudeUnit(
            name="Quart",
            abbreviation="qt",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="https://en.wikipedia.org/wiki/Quart#US_liquid_quart",
        )
        db.session.add(us_quart)
        db.session.commit()

        us_pint = MagnitudeUnit(
            name="Pint",
            abbreviation="pt",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_pint)
        db.session.commit()

        us_cup = MagnitudeUnit(
            name="Cup",
            abbreviation="cp",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_cup)
        db.session.commit()

        us_fluid_ounce = MagnitudeUnit(
            name="Fluid Ounce",
            abbreviation="fl oz",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_fluid_ounce)
        db.session.commit()

        us_pound = MagnitudeUnit(
            name="Pound",
            abbreviation="lb",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Mass")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_pound)
        db.session.commit()

        us_ounce = MagnitudeUnit(
            name="Ounce",
            abbreviation="oz",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Mass")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Customary Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_ounce)
        db.session.commit()

        # US nutrition labeling units # TODO: consider calling these units US FDA units
        us_food_fluid_ounce = MagnitudeUnit(
            name="Fluid Ounce",
            abbreviation="fl oz",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Nutrition Labeling Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_food_fluid_ounce)
        db.session.commit()

        us_food_cup = MagnitudeUnit(
            name="Cup",
            abbreviation="cp",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="United States Nutrition Labeling Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(us_food_cup)
        db.session.commit()

        # SI units
        si_litre = MagnitudeUnit(
            name="Litre",
            abbreviation="l",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="International System of Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(si_litre)
        db.session.commit()

        si_milliliter = MagnitudeUnit(
            name="Millilitre",
            abbreviation="ml",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Volume")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="International System of Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(si_milliliter)
        db.session.commit()

        si_kilogram = MagnitudeUnit(
            name="Kilogram",
            abbreviation="kg",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Mass")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="International System of Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(si_kilogram)
        db.session.commit()

        si_gram = MagnitudeUnit(
            name="Gram",
            abbreviation="g",
            magnitude_unit_type_id=MagnitudeUnitType.query.filter_by(name="Mass")
            .first()
            .id,
            magnitude_unit_system_id=MagnitudeUnitSystem.query.filter_by(
                name="International System of Units"
            )
            .first()
            .id,
            wikidata="",
            wikipedia="",
        )
        db.session.add(si_gram)
        db.session.commit()
        flash("Populating magnitude units ... Done.", "success")
    else:
        flash("Magnitude units already created. Doing nothing.", "warning")

    return redirect(url_for("setup"))


@app.route("/import_stores", methods=["GET", "POST"])
@login_required
@access.permission_required("Add Store")
def import_stores():
    form = ImportStoresForm()

    if not form.validate_on_submit():
        return render_template("import_stores.html", form=form)
    elif not geocode_within_boundary(
        geocode_boundary(form.location.data),
        extract_key_value_pairs(form.key_value_pairs.data),
    ):
        flash("OpenStreetMap relation not found for that location", "warning")
        return render_template("import_stores.html", form=form)
    else:
        try:
            overpass_results = geocode_within_boundary(
                geocode_boundary(form.location.data),
                extract_key_value_pairs(form.key_value_pairs.data),
            )
        except:
            flash("Overpass API returned an error", "danger");
            return render_template("import_stores.html", form=form)
        cleaned_results = (
            overpass_results.nodes + overpass_results.ways + overpass_results.relations
        )
        osm_ids_in_db = [
            store.store_versions[0].osm_id
            for store in Store.query.join(StoreVersion).all()
        ]

        store_import_count = 0

        for result in cleaned_results:
            if result.id not in osm_ids_in_db:

                store_import_count += 1

                if hasattr(result, "lat"):
                    lat = result.lat
                    lon = result.lon
                elif hasattr(result, "center_lat"):
                    lat = result.center_lat
                    lon = result.center_lon
                else:
                    lat = 36
                    lon = -51

                store = Store()
                db.session.add(store)
                db.session.flush()

                street_name = result.tags.get("addr:street", None)
                nomcode = None
                if street_name == None:
                    nomcode = reverse_geocode(result).json()
                    if "road" in nomcode[0]["address"].keys():
                        street_name = nomcode[0]["address"]["road"]
                    elif "footway" in nomcode[0]["address"].keys():
                        street_name = nomcode[0]["address"]["footway"]
                        
                addr_city = result.tags.get("addr:city", None)
                if addr_city == None:
                    if nomcode == None:
                        nomcode = reverse_geocode(result).json()
                    if "city" in nomcode[0]["address"].keys():
                        addr_city = nomcode[0]["address"]["city"]
                    elif "village" in nomcode[0]["address"].keys():
                        addr_city = nomcode[0]["address"]["village"]
                    elif "hamlet" in nomcode[0]["address"].keys():
                        addr_city = nomcode[0]["address"]["hamlet"]
                        
                store_version = StoreVersion(
                    store_id=store.id,
                    user_id=current_user.id,
                    name=result.tags.get("name", None),
                    # brand_id = None
                    # category_id = None
                    location=f"POINT({lat} {lon})",
                    osm_id=result.id,
                    addr_housenumber=result.tags.get("addr:housenumber", None),
                    addr_housename=result.tags.get("addr:housename", None),
                    addr_conscriptionnumber=result.tags.get(
                        "addr:conscriptionnumber", None
                    ),
                    addr_street=street_name,
                    addr_place=result.tags.get("addr:place", None),
                    addr_postcode=result.tags.get("addr:postcode", None),
                    addr_city=addr_city,
                    addr_country=result.tags.get("addr:country", None),
                    addr_hamlet=result.tags.get("addr:hamlet", None),
                    addr_suburb=result.tags.get("addr:suburb", None),
                    addr_subdistrict=result.tags.get("addr:subdistrict", None),
                    addr_province=result.tags.get("addr:province", None),
                    addr_state=result.tags.get("addr:state", None),
                    addr_door=result.tags.get("addr:door", None),
                    addr_unit=result.tags.get("addr:unit", None),
                    addr_floor=result.tags.get("addr:floor", None),
                    addr_block=result.tags.get("addr:block", None),
                )
                db.session.add(store_version)
                db.session.commit()

        flash(f"Imported {store_import_count} stores from {form.location.data}", "success")

        return redirect(url_for("setup"))

@app.route("/view_store/<store_id>")
@access.permission_required("View Store")
def view_store(store_id):
    store = (
        Store.query.join(StoreVersion).filter(Store.id == store_id).first()
    )
    latest_version = store.store_versions[0]

    location = latest_version.location
    if not location:
        location = WKTElement(app.config["DEFAULT_LOCATION_EWKT"], extended=True)
    
    return render_template(
        "view_store.html",
        store = latest_version,
        location = to_shape(location),
        mapbox_api_key=app.config["MAPBOX_API_KEY"],
    )

#@access.permission_required("View Store")
@app.route("/find_stores")
def find_stores():
    form = FindStoresForm(formdata=request.args, meta={'csrf': False})
    if not form.validate():
        if request.headers.get("Accept") == "Application/JSON":
            return '{ "length": "0" }'
        else:
            return render_template("find_stores.html", form=form)
    latitude = form.latitude.data
    longitude = form.longitude.data
    if not form.radius.data:
        radius = 400
    else:
        radius = form.radius.data
    stores = (
        StoreVersion
        .query
        .order_by("store_id", StoreVersion.change_dtm.desc())
        .distinct("store_id")
        .filter(ST_DWithin(StoreVersion.location, f"POINT({latitude} {longitude})", radius))
        .all()
    )
    if request.headers.get("Accept") == "Application/JSON":
        stores_json = []
        for store in stores:
            point = to_shape(store.location)
            longitude = point.y + 0
            latitude = point.x + 0
            stores_json.append({
                "name": store.name,
                "addr_street": store.addr_street,
                "addr_housenumber": store.addr_housenumber,
                "store_id": store.store_id,
                "latitude": latitude,
                "longitude": longitude
            })
        data = {
            "length": len(stores),
            "stores": stores_json
        }
        return jsonify(data);
    return render_template(
        "find_stores.html",
        form=form,
        stores=stores
    )


@app.route("/import_product/<barcode>")
@access.permission_required("Add Product")
def import_product(barcode):
    if current_user.is_authenticated:
        if (
            not Product.query.join(ProductVersion)
            .filter(ProductVersion.barcode == barcode)
            .first()
        ):
            off_result = openfoodfacts.api(barcode).get("product")
            if off_result:
                name = off_result.get("product_name", None)
                quantity = off_result.get("quantity", None)
                categories = off_result.get("categories", None)
                if categories:
                    categories = categories.split(",")
                    for category_name in categories:
                        category_name = category_name.strip()
                        existing_off_category = OpenFoodFactsCategory.query.filter_by(name=category_name).first()
                        if existing_off_category == None:
                            open_food_facts_category = OpenFoodFactsCategory(
                                name=category_name
                            )
                            product_attribute = ProductAttribute.query.filter_by(name=category_name).first()
                            if product_attribute == None:
                                new_product_attribute = ProductAttribute(
                                    name=category_name
                                )
                                db.session.add(new_product_attribute)
                                db.session.flush()
                                product_attribute = ProductAttribute.query.filter_by(name=category_name).first()
                            open_food_facts_category.product_attributes.append(product_attribute)
                            db.session.add(open_food_facts_category)
                            db.session.flush()

                if name and quantity:
                    match = re.search("[a-zA-Z]+", quantity)
                    magnitude = re.search("^\d+\.?\d*", quantity)
                    if magnitude:
                        magnitude = magnitude[0]
                    else:
                        magnitude = off_result.get("product_quantity", None)
                    if match:
                        unit_abbreviation = match[0]
                        units = MagnitudeUnit.query.filter(
                            MagnitudeUnit.abbreviation.ilike(unit_abbreviation)
                        ).first()
                        if units:
                            product = Product()
                            db.session.add(product)
                            db.session.flush()
                            product_version = ProductVersion(
                                product_id=product.id,
                                user_id=current_user.id,
                                barcode=barcode,
                                category_id=None,
                                name=off_result.get("product_name", None),
                                magnitude=magnitude,
                                magnitude_unit_id=units.id,
                                remote_image_url=off_result.get(
                                    "image_front_url", None
                                ),
                            )
                            if categories:
                                for category_name in categories:
                                    category_name = category_name.strip()
                                    open_food_facts_category = OpenFoodFactsCategory.query.filter_by(name=category_name).first()
                                    product_version.open_food_facts_categories.append(open_food_facts_category)
                                    product_version.category_id = open_food_facts_category.product_category_id
                                    for attribute in open_food_facts_category.product_attributes:
                                        product_version.product_attributes.append(attribute)
                            db.session.add(product_version)
                            db.session.commit()
                            flash("Product imported from OpenFoodFacts", "success")
                            return redirect(
                                f"{request.referrer}?query={quote_plus(barcode)}"
                            )
                        else:
                            flash(
                                f"Product quantity from OpenFoodFacts has an unrecognized unit ({unit_abbreviation}). Doing nothing.", "warning"
                            )
                            return redirect(request.referrer)
                    else:
                        flash(
                            "Product quantity from OpenFoodFacts does not have a unit. Doing nothing.", "warning"
                        )
                        return redirect(request.referrer)
                else:
                    flash(
                        "Product data from OpenFoodFacts is too incomplete to import. Doing nothing.", "warning"
                    )
                    return redirect(request.referrer)
            else:
                flash(
                    "Product is not in this database or the OpenFoodFacts database. Doing nothing.", "warning"
                )
                return redirect(request.referrer)
        else:
            flash("Product already in database. Doing nothing.", "warning")
            return redirect(request.referrer)
    else:
        # TODO perhaps redirect to login page here with 'next' parameter that
        # redirects back after login
        flash(
            "Product needs to be imported from OpenFoodFacts, but a user must be logged in to do this. Doing nothing.", "warning"
        )
        return redirect(request.referrer)


# This is written for the bare minimum of importing the initial one-to-one OFF
# categories/attributes
@app.route("/import_off_categories")
@login_required
@access.role_required("Administrator")
def import_off_categories():
    print("ERROR")
    

@app.route("/view_roles")
@access.permission_required("View Role")
#@login_required
def view_roles():
    roles = AccessRole.query.all()
    return render_template("view_roles.html", roles=roles)

@app.route("/view_role/<role_id>")
@access.permission_required("View Role")
def view_role(role_id):
    role = AccessRole.query.filter(AccessRole.id == role_id).first()
    return render_template("view_role.html", role=role)

@app.route("/add_role", methods=["GET", "POST"])
@login_required
@access.permission_required("Add Role")
def add_role():
    form = AddRoleForm()
    if not form.validate_on_submit():
        return render_template("add_role.html", form=form)
    else:
        role = AccessRole(
            name = form.name.data
            )
        # introspecting into and iterating through form object so each form field doesn't need to be checked with a different if statement
        for field_str in dir(form):
            if "submit" not in field_str:
                field_obj = getattr(form, field_str)
                data_obj = getattr(field_obj, "data", False)
                if data_obj == True:
                    permission = Permission.query.filter(Permission.name == (str(field_obj.description))).first()
                    if permission:
                        role.permissions.append(permission)
        db.session.add(role)
        db.session.commit()
        return redirect(f"/view_role/{role.id}")

@app.route("/populate_initial_roles", methods=["GET", "POST"])
def populate_initial_roles():
    if len(AccessRole.query.all()) == 0:

        # populate administrator role with administrator permission from
        # access.py
        administrator_role = AccessRole(
            name = "Administrator" 
        )        
        for permission in Permission.query.all():
            if permission.name in access.administrator_role_initial_permissions:
                administrator_role.permissions.append(permission)
        db.session.add(administrator_role)
        
        # populate basic role with basic permission list from access.py
        basic_role = AccessRole(
            name = "Basic" 
        )
        for permission in Permission.query.all():
            if permission.name in access.basic_role_initial_permissions:
                basic_role.permissions.append(permission)
        db.session.add(basic_role)
        
        db.session.commit()
        flash("Populating initial roles ... Done", "success")

    else:
        flash("Initial roles already populated. Doing nothing.", "warning")

    return redirect(request.referrer)


@app.route("/edit_role/<role_id>", methods=['GET', 'POST'])
@login_required
@access.permission_required("Edit Role")
def edit_role(role_id):
    old_role = AccessRole.query.filter(AccessRole.id == role_id).first()
    form = EditRoleForm(role_id = old_role.id)
    
    if not form.validate_on_submit():

        # populate form with current data from role
        form.name.data = old_role.name 
        for permission in old_role.permissions:
            formatted_permission_name = permission.name.lower().replace(" ", "_")
            setattr(getattr(form, formatted_permission_name), "data", True)
        return render_template("edit_role.html", form=form, role_id=role_id)
    else:
        # clearing old permissions from object before new ones are added
        old_role.permissions.clear()
        db.session.flush()
        # introspecting into and iterating through form object so each form field doesn't need to be checked with a different if statement
        for field_str in dir(form):
            if "submit" not in field_str:
                field_obj = getattr(form, field_str)
                data_obj = getattr(field_obj, "data", None)
                if data_obj in [True, False]:
                    permission = Permission.query.filter(Permission.name == (str(field_obj.description))).first()
                    if permission:
                        if data_obj == True and permission not in old_role.permissions: 
                            old_role.permissions.append(permission)
        old_role.name = form.name.data
        db.session.add(old_role)
        db.session.commit()
        return redirect(f"/view_role/{old_role.id}")


@app.route("/populate_permissions")
def populate_permissions():
    
    extant_permissions = Permission.query.all()
    if len(extant_permissions) == 0:
        
        new_permissions = [Permission(name = "View Product"),
            Permission(name = "Add Product"),
            Permission(name = "Edit Product"),
            Permission(name = "Delete Product"),
            Permission(name = "View Store"),
            Permission(name = "Add Store"),
            Permission(name = "Edit Store"),
            Permission(name = "Delete Store"),
            Permission(name = "View Price"),
            Permission(name = "Add Price"),
            Permission(name = "Edit Price"),
            Permission(name = "Delete Price"),
            Permission(name = "View User"),
            Permission(name = "Add User"),
            Permission(name = "Edit User"),
            Permission(name = "Delete User"),
            Permission(name = "View Role"),
            Permission(name = "Add Role"),
            Permission(name = "Edit Role"),
            Permission(name = "Delete Role")
        ]
        # store categories, product categories, and currencies are fixed at deployment, so cannot be changed and do not require associated permissions
        # store and product brands are still a matter of discussion

        db.session.add_all(new_permissions)
        db.session.commit()

        flash("Populating permissions ... Done", "success")
    else:
        flash("Permissions already created. Doing nothing.", "warning")

    return redirect(request.referrer)


@app.route("/.well-known/acme-challenge/<path:filename>")
def acme_challenge(filename):
    return send_from_directory("static/.well-known/acme-challenge/", filename)


# This displays the license information for javascript
@app.route("/freedom")
def freedom():
    return render_template("freedom.html")
